```
docker build . -t mentor:latest
docker run -id --rm --privileged --name=mentor mentor:latest
docker exec -it mentor bash
su mentor-user
minikube start
```